<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('RepresentanteXid',
    [
        'login' => 'tns:login',
        'idRepresentante' => 'xsd:integer'
    ],
    array('RepresentanteXidResult'=>'tns:RepresentanteXidResult')
);