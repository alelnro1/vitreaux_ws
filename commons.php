<?php
/**
 * Funciones comunes a todos los archivos (Se pueden utilizar en todos lados)
 */

function login($auth)
{
    // TODO: En vez de true poner la validacion del usuario y password
    if ($auth['Usuario'] == "admin" && $auth['Password'] == "vitr_admin") {
        return true;
    } else {
        $aReturn = array(
            'Errs' => array(
                'Codigo' => 2000,
                'Msg' => 'Error de Login'
            ),
        );

        return $aReturn;
    }
}
function dameIdPerfil($nombre){
    global $db;

    return $db->rawQueryOne('select per_id from perfil where per_nombre="'. $nombre .'"');

}
function actualizarUsuario($usuario, $correo, $activo,$perfil, $usuarioWeb)
{
    global $db;

    $data = [
        'usu_email' => $correo,
        'usu_activo' => $activo,
        'usu_nombre' => $usuarioWeb,
        'usu_per_id' => dameIdPerfil($perfil)['per_id']
    ];

    $db->where('usu_login', $usuarioWeb);

    $db->update('usuario', $data);

    // Busco al usuario recien actualizado
    $usuario = $db->rawQueryOne('select * from usuario where usu_login="' . $usuario['usu_login'] . '"');

    return $usuario;
}
function insertarUsuario($usuarioWeb, $contrasena, $correo, $activo, $perfil)
{


    global $db;

    $data = array(
        'usu_login' => $usuarioWeb,
        'usu_clave' => $contrasena,
        'usu_email' => $correo,
        'usu_activo' => $activo,
        'usu_nombre' => $usuarioWeb,
        'usu_per_id' => dameIdPerfil($perfil)['per_id'],
    );

    $id = $db->insert('usuario', $data);

    // Busco al usuario recien insertado
    $usuario = $db->rawQueryOne('select * from usuario where usu_login="' . $usuarioWeb . '"');

    return $usuario;
}