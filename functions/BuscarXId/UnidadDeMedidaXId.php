<?php

function UnidadeDeMedidaXid($auth, $idUnidadMedida)
{
    $login = login($auth);

    if ($login !== true){
        return $login;
    }

    // Digo que voy a utilizar la variable global $mysql
    global $db;

    // Armo un array con los parametros y sus valores
    //$array = array_combine($nombres_parametros, $valores_parametros);

    // Inicializo la variable resultado
    $aReturn = [];

    //foreach ($array as $key => $value) {
        // Busco en la base de datos
        $unidad_medida  = $db->rawQueryOne ('select * from unidad_medida where ume_id=' . $idUnidadMedida);

        if ($unidad_medida) {
            $aReturn = array(
                'Errs' => array(
                    'Codigo' => 0,
                    'Msg' => ''
                ),
                'idUnidadDeMedida' => utf8_decode($unidad_medida['ume_id']),
                'unidadDeMedida' => utf8_decode($unidad_medida['ume_nombre']),
                'baja' => utf8_decode($unidad_medida['ume_baja'])
            );
        } else {
            $aReturn = array(
                'Errs' => array(
                    'Codigo' => 1003,
                    'Msg' => 'No existe'
                ),
                'idUnidadDeMedida' => 0,
                'baja' => false
            );
        }
    //}

    return $aReturn;
}