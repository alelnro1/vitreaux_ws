<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('ProdCod_AM',
    [
        'login' => 'tns:login',
        'Codigo' => 'xsd:string',
        'Nombre' => 'xsd:string',
        'Abreviado' => 'xsd:string'
    ],
    array('ProdCod_AMResult' => 'tns:ProdCod_AMResult')
);