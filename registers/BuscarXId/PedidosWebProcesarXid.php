<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('PedidosWebProcesarXid',
    [
        'login' => 'tns:login',
        'idPedido' => 'xsd:integer'
    ],
    array('PedidosWebProcesarXidResult'=>'tns:PedidosWebProcesarXidResult')
);