<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('Vendedor_UsuarioWebXid',
    [
        'login' => 'tns:login',
        'idVendedor' => 'xsd:integer'
    ],
    array('Vendedor_UsuarioWebXidResult' => 'tns:Vendedor_UsuarioWebXidResult')
);

