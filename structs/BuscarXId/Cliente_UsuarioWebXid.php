<?php

/** En este archivo se definen las responses del WS */

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->wsdl->addComplexType('Cliente_UsuarioWebXidResult','complexType','array','','SOAP-ENC:Array',
    array(
        array(
            array('ref' => 'SOAP-ENC:arrayType',
                'wsdl:arrayType' => 'tns:Cliente_UsuarioWebXid[]'
            )
        )
    ),
    'tns:Cliente_UsuarioWebXid');

// Creamos el tipo de registro
$server->wsdl->addComplexType('Cliente_UsuarioWebXid','complexType','struct','all','',
    array(
        'Errs' => array('name'=>'Errs','type'=>'tns:Errs'),
        'IdUsuario' => array('name'=>'IdUsuario','type'=>'xsd:integer'),
        'Usuario' => array('name'=>'Usuario','type'=>'xsd:string'),
        'Nombre' => array('name'=>'Nombre','type'=>'xsd:string'),
        'Correo' => array('name'=>'Correo','type'=>'xsd:integer'),
        'Activo' => array('name'=>'Activo','type'=>'xsd:boolean'),
        'id' => array('name'=>'id','type'=>'xsd:integer'),
        'IdPerfil' => array('name'=>'IdPerfil','type'=>'xsd:integer')
    ));
