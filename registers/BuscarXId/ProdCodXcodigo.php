<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('ProdCodXcodigo',
    [
        'login' => 'tns:login',
        'Codigo' => 'xsd:integer'
    ],
    array('ProdCodXcodigoResult'=>'tns:ProdCod')
);