<?php

function Cliente_DeleteXid($auth, $idCliente)
{
    $login = login($auth);

    if ($login !== true){
        return $login;
    }

    // Digo que voy a utilizar la variable global $mysql
    global $db;

    // Armo un array con los parametros y sus valores
    //$array = array_combine($nombres_parametros, $valores_parametros);

    // Inicializo la variable resultado
    $aReturn = [];

    //foreach ($array as $key => $value) {
        // Busco en la base de datos
        $cliente = $db->rawQueryOne ('select * from cliente where cli_id=' . $idCliente);

        // Lo encontre
        if ($cliente) {
            // Lo elimino
            $db->where('cli_id', $cliente['cli_id']);

            if ($db->delete('cliente')) {
                $cod = 2;
                $msg = 'Cliente ' . $idCliente . ' eliminado';
            } else {
                $cod = 3;
                $msg = 'El cliente ' . $idCliente . ' no se pudo eliminar';
            }
        } else {
            $cod = 1;
            $msg = 'Cliente inexistente';
        }
    //}

    $aReturn = array(
        'Errs' => array(
            'Codigo' => $cod,
            'Msg' => $msg
        )
    );

    return $aReturn;
}