<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('Domicilio_AM',
    [
        'login' => 'tns:login',
        'idDomicilio' => 'xsd:integer',
        'IdCliente' => 'xsd:integer',
        'domicilio' => 'xsd:string'
    ],
    array('Domicilio_AMResult' => 'tns:Domicilio_AMResult')
);