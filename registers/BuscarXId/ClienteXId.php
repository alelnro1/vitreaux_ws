<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('ClienteXid',
    [
        'login' => 'tns:login',
        'idCliente' => 'xsd:integer'
    ],
    array('ClienteXidResult' => 'tns:ClienteXidResult')
);

