<?php

function Representante_AM($auth, $idRepresentante, $nombre)
{
    $login = login($auth);

    if ($login !== true){
        return $login;
    }

    // Digo que voy a utilizar la variable global $mysql
    global $db;

    // Inicializo la variable resultado
    $aReturn = [];

    // Busco en la base de datos
    $rep_db = $db->rawQueryOne ('select * from representante where rep_id=' . $idRepresentante);

    if ($rep_db) {
        // Existe => ACTUALIZO
        $rep_db = actualizarRepresentante($rep_db, $idRepresentante, $nombre);
        $msg_success = 'Modificacion exitosa';
    } else {
        // No existe => INSERTO
        $rep_db = insertarRepresentante($idRepresentante, $nombre);
        $msg_success = 'Alta exitosa';
    }

    if ($db->getLastErrno() === 0) {
        $aReturn = array(
            'Errs' => array(
                'Codigo' => 0,
                'Msg' => $msg_success
            ),
            'idRepresentante' => utf8_decode($rep_db['rep_id']),
            'representante' => utf8_decode($rep_db['rep_nombre']),
            'baja' => utf8_decode($rep_db['rep_baja'])
        );
    } else {
        $aReturn = array(
            'Errs' => array(
                'Codigo' => $db->getLastErrno(),
                'Msg' => utf8_decode($db->getLastError())
            ),
            'idRepresentante' => utf8_decode($rep_db['rep_id']),
            'representante' => utf8_decode($rep_db['rep_nombre']),
            'baja' => utf8_decode($rep_db['rep_baja'])
        );
    }

    return $aReturn;
}

function actualizarRepresentante($rep, $idRepresentante, $nombre)
{
    global $db;

    $db->where('rep_id', $rep['rep_id'])
        ->update('representante', [
            'rep_id' => $idRepresentante,
            'rep_nombre' => $nombre,
        ]);

    // Busco al domicilio recien actualizado
    $rep = $db->rawQueryOne ('select * from representante where rep_id=' . $idRepresentante);

    return $rep;
}

function insertarRepresentante($idRepresentante, $nombre)
{
    global $db;

    $data = array(
        'rep_id' => $idRepresentante,
        'rep_nombre' => $nombre,
    );

    $id = $db->insert('representante', $data);

    // Busco al domicilio recien insertado
    $rep = $db->rawQueryOne ('select * from representante where rep_id=' . $idRepresentante);

    return $rep;
}