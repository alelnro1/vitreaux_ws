<?php

function Cliente_AM($auth, $idCliente, $codigoCliente, $idVendedor , $idRepresentante, $razonSocial, $EstaObservado)
{
    $login = login($auth);

    if ($login !== true){
        return $login;
    }

    // Digo que voy a utilizar la variable global $mysql
    global $db;

    // Inicializo la variable resultado
    $aReturn = [];

    // Busco en la base de datos
    $cliente = $db->rawQueryOne ('select * from cliente where cli_id=' . $idCliente);

    if ($cliente) {
        // Existe => ACTUALIZO
        $cliente = actualizarCliente($cliente, $idCliente, $codigoCliente, $idVendedor , $idRepresentante, $razonSocial, $EstaObservado);
        $msg_success = 'Modificacion exitosa';
    } else {
        // No existe => INSERTO
        $cliente = insertarCliente($idCliente, $codigoCliente, $idVendedor , $idRepresentante, $razonSocial, $EstaObservado);
        $msg_success = 'Alta exitosa';
    }

    if ($db->getLastErrno() === 0) {
        $aReturn = array(
            'Errs' => array(
                'Codigo' => 0,
                'Msg' => $msg_success
            ),
            'idCliente' => utf8_decode($cliente['cli_id']),
            'razonSocial' => utf8_decode($cliente['cli_razon_social']),
            'codigoCliente' => utf8_decode($cliente['cli_codigo']),
            'idVendedor' => utf8_decode($cliente['cli_ven_id']),
            'idRepresentante' => utf8_decode($cliente['cli_rep_id']),
            'estaObservado' => utf8_decode($cliente['cli_esta_observado']),
            'Baja' => utf8_decode($cliente['cli_baja'])
        );
    } else {
        $aReturn = array(
            'Errs' => array(
                'Codigo' => $db->getLastErrno(),
                'Msg' => utf8_decode($db->getLastError())
            ),
            'idCliente' => utf8_decode($cliente['cli_id']),
            'razonSocial' => utf8_decode($cliente['cli_razon_social']),
            'codigoCliente' => utf8_decode($cliente['cli_codigo']),
            'idVendedor' => utf8_decode($cliente['cli_ven_id']),
            'idRepresentante' => utf8_decode($cliente['cli_rep_id']),
            'estaObservado' => utf8_decode($cliente['cli_esta_observado']),
            'Baja' => utf8_decode($cliente['cli_baja'])
        );
    }

    return $aReturn;
}

function actualizarCliente($cliente, $idCliente, $codigoCliente, $idVendedor , $idRepresentante, $razonSocial, $EstaObservado)
{
    global $db;

    $db->where('cli_id', $cliente['cli_id'])
        ->update('cliente', [
            'cli_ven_id' => $idVendedor,
            'cli_rep_id' => $idRepresentante,
            'cli_razon_social' => $razonSocial,
            'cli_codigo' => $codigoCliente,
            'cli_esta_observado' => $EstaObservado

        ]);

    // Busco al cliente recien actualizado
    $cliente = $db->rawQueryOne ('select * from cliente where cli_id=' . $idCliente);

    return $cliente;
}

function insertarCliente($idCliente, $codigoCliente, $idVendedor , $idRepresentante, $razonSocial, $EstaObservado)
{
    global $db;

    $data = array(
        'cli_id' => $idCliente,
        'cli_ven_id' => $idVendedor,
        'cli_rep_id' => $idRepresentante,
        'cli_razon_social' => $razonSocial,
        'cli_codigo' => $codigoCliente,
        'cli_esta_observado' => $EstaObservado
    );

    $id = $db->insert('cliente', $data);

    // Busco al cliente recien insertado
    $cliente = $db->rawQueryOne ('select * from cliente where cli_id=' . $idCliente);

    return $cliente;
}