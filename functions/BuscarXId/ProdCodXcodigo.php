<?php

function ProdCodXcodigo($auth, $codigo)
{
    $login = login($auth);

    if ($login !== true){
        return $login;
    }

    // Digo que voy a utilizar la variable global $mysql
    global $db;

    // Inicializo la variable resultado
    $aReturn = [];

    // Busco en la base de datos
    $producto = $db->rawQueryOne ('select * from producto where pro_codigo=' . $codigo);

    if ($producto) {
        $aReturn = array(
            'Errs' => array(
                'Codigo' => 0,
                'Msg' => ''
            ),
            'Codigo' => utf8_decode($producto['pro_codigo']),
            'Nombre' => utf8_decode($producto['pro_nombre']),
            'Abreviado' => utf8_decode(""),
        );
    } else {
        $aReturn = array(
            'Errs' => array(
                'Codigo' => 1003,
                'Msg' => 'No existe'
            )
        );
    }



    return $aReturn;
}