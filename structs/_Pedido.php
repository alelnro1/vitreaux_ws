<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->wsdl->addComplexType('_Pedido', 'complexType', 'struct', 'all', '',
    array(
        'Errs' => array('name'=>'Errs','type'=>'tns:Err'),
        'Pedido_Id' => array('name'=>'Pedido_Id','type'=>'xsd:integer'),
        'Pedido_idCliente' => array('name'=>'Pedido_idCliente','type'=>'xsd:integer'),
        'Pedido_FechaInicio' => array('name'=>'Pedido_FechaInicio','type'=>'xsd:dateTime'),
        'Pedido_IdDomicilio' => array('name'=>'Pedido_IdDomicilio','type'=>'xsd:integer'),
        'Pedido_IdUsuario' => array('name'=>'Pedido_IdUsuario','type'=>'xsd:integer'),
        'Pedido_Procesada' => array('name'=>'Pedido_Procesada','type'=>'xsd:boolean'),
        'Pedido_FechaProcesada' => array('name'=>'Pedido_FechaProcesada','type'=>'xsd:dateTime'),
        'Pedido_Observaciones' => array('name'=>'Pedido_Observaciones','type'=>'xsd:string'),
        'Pedido_OrdenDeCompra' => array('name'=>'Pedido_OrdenDeCompra','type'=>'xsd:string')

    ));