<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('PedidosWebRenglones',
    [
        'login' => 'tns:login',
        'IdPedido' => 'xsd:integer'
    ],
    array('PedidosWebRenglonesResult'=>'tns:PedidosWebRenglonesResult')
);