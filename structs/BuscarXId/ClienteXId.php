<?php

/** En este archivo se definen las responses del WS */

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->wsdl->addComplexType('ClienteXidResult','complexType','array','','SOAP-ENC:Array',
    array(
        array(
            array('ref' => 'SOAP-ENC:arrayType',
                'wsdl:arrayType' => 'tns:Cliente[]'
            )
        )
    ),
    'tns:Cliente');

// Creamos el tipo de registro
$server->wsdl->addComplexType('Cliente','complexType','struct','all','',
    array(
        'Errs' => array('name'=>'Errs','type'=>'tns:Errs'),
        'idCliente' => array('name'=>'idCliente','type'=>'xsd:integer'),
        'razonSocial' => array('name'=>'razonSocial','type'=>'xsd:string'),
        'codigoCliente' => array('name'=>'codigoCliente','type'=>'xsd:string'),
        'idVendedor' => array('name'=>'idVendedor','type'=>'xsd:integer'),
        'idRepresentante' => array('name'=>'idRepresentante','type'=>'xsd:integer'),
        'estaObservado' => array('name'=>'estaObservado','type'=>'xsd:boolean'),
        'Baja' => array('name'=>'Baja','type'=>'xsd:boolean')
    ));
