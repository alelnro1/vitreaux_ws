<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('PedidosWebXid',
    [
        'login' => 'tns:login',
        'Id' => 'xsd:integer'
    ],
    array('PedidosWebXidResult'=>'tns:PedidosWebXidResult')
);