<?php

function PedidosWebXid($auth, $idFamilia)
{
	mail("julycm92@gmail.com", 'Entro a PedidosWebProcesarXid\n', 'Entro a PedidosWebXid\n');
	
    $login = login($auth);

    if ($login !== true){
        return $login;
    }

    // Digo que voy a utilizar la variable global $mysql
    global $db;

    // Armo un array con los parametros y sus valores
    //$array = array_combine($nombres_parametros, $valores_parametros);

    // Inicializo la variable resultado
    $aReturn = [];

    //foreach ($array as $key => $value) {
        // Busco en la base de datos
        $query = "SELECT * FROM pedido LEFT JOIN domicilio ON ped_dom_id = dom_id WHERE ped_id='" . $idFamilia . "'";

        $pedido = $db->rawQueryOne ($query);

        if ($pedido) {
            $aReturn = array(
                'Errs' => array(
                    'Codigo' => 0,
                    'Msg' => ''
                ),
                'Pedido_Id' => utf8_decode($pedido['ped_id']),
                'Pedido_idCliente' => utf8_decode($pedido['dom_cli_id']),
                'Pedido_FechaInicio' => utf8_decode(date("Y-m-d", strtotime($pedido['ped_fecha_alta']))),
                'Pedido_IdDomicilio' => utf8_decode($pedido['ped_dom_id']),
                'Pedido_IdUsuario' => utf8_decode($pedido['ped_usu_id']),
                'Pedido_Procesada' => utf8_decode($pedido['ped_procesado']),
                'Pedido_FechaProcesada' => utf8_decode(date("Y-m-d", strtotime($pedido['ped_fecha_alta']))),
                'Pedido_Observaciones' => utf8_decode($pedido['ped_observaciones']),
                'Pedido_OrdenDeCompra' => utf8_decode($pedido['ped_orden_compra'])
            );
        } else {
            $aReturn = array(
                'Errs' => array(
                    'Codigo' => 1000,
                    'Msg' => 'No exite pedido con ese Id'
                ),
                'Pedido_Id' => 0,
                'Pedido_idCliente' => 0,
                'Pedido_FechaInicio' => date("Y-m-d", time()),
                'Pedido_IdDomicilio' => 0,
                'Pedido_IdUsuario' => 0,
                'Pedido_Procesada' => false,
                'Pedido_FechaProcesada' => date("Y-m-d", time()),
                'Pedido_Observaciones' => NULL,
                'Pedido_OrdenDeCompra' => NULL
            );
        }


    //}

    return $aReturn;
}