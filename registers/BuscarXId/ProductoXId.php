<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('ProductoXId',
    [
        'login' => 'tns:login',
        'idProducto' => 'xsd:integer'
    ],
    array('ProductoXidResult'=>'tns:ProductoXidResult')
);