<?php

require_once "lib/nusoap.php";

$client = new nusoap_client("http://doublepoint.com.ar/vitreaux_ws/server.php?wsdl");

$error = $client->getError();

if ($error) {
    echo "<h2>Constructor error</h2><pre>" . $error . "</pre>";
}

/** Ejemplo de Cliente */
    $result = $client->call("Cliente_DeleteXid", [
        'idCliente' => '853'
    ]);

/** Ejemplo de DomicilioXId */
    $result = $client->call("Domicilio_DeleteXid", [
        'idDomicilio' => '25'
    ]);

/** Ejemplo de Domicilio_DeleteXidCliente */
    $result = $client->call("Domicilio_DeleteXidCliente", [
        'idCliente' => '827'
    ]);

/** Ejemplo de Familia_DeleteXid */
    $result = $client->call("Familia_DeleteXid", [
        'idFamilia' => '23'
    ]);

/** Ejemplo de ProdCod_DeleteXcodigo */
    $result = $client->call("ProdCod_DeleteXcodigo", [
        'codProducto' => '1010100'
    ]);

/** Ejemplo de Producto_DeleteXid */
    $result = $client->call("Producto_DeleteXid", [
        'idProducto' => '2'
    ]);

/** Ejemplo de Representante_DeleteXid */
    $result = $client->call("Representante_DeleteXid", [
        'idRepresentante' => '1'
    ]);

/** Ejemplo de UnidadesDeMedida_DeleteXid */
    $result = $client->call("UnidadesDeMedida_DeleteXid", [
        'idUnidadDeMedida' => '17'
    ]);


/** Ejemplo de UnidadDeMedidaXId */
    $result = $client->call("Vendedor_DeleteXid", [
        'idVendedor' => '1'
    ]);


echo "<h2>Request</h2>";
echo "<pre>" . htmlspecialchars($client->request) . "</pre>";
echo "<h2>Response</h2>";
echo "<pre>" . htmlspecialchars($client->response) . "</pre>";
