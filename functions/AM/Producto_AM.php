<?php


function Producto_AM($auth, $IdProducto, $Codigo, $Nombre, $idFamilia, $idUnidadDeMedida, $capacidad, $cantidadPack)
{
    $login = login($auth);

    if ($login !== true){
        return $login;
    }

    // Digo que voy a utilizar la variable global $mysql
    global $db;

    // Inicializo la variable resultado
    $aReturn = [];

    // Busco en la base de datos
    $producto = $db->rawQueryOne ("select * from producto where pro_codigo='" . $Codigo . "'");

    if ($producto) {
        // Existe => ACTUALIZO
        $producto = actualizarProductoAM($IdProducto, $producto, $Codigo, $Nombre, $idFamilia, $idUnidadDeMedida, $capacidad, $cantidadPack);
        $msg_success = 'Modificacion exitosa';
    } else {
        // No existe => INSERTO
        $producto = insertarProductoAM($IdProducto, $Codigo, $Nombre, $idFamilia, $idUnidadDeMedida, $capacidad, $cantidadPack);
        $msg_success = 'Alta exitosa';
    }

    if ($db->getLastErrno() === 0) {
        $aReturn = array(
            'Errs' => array(
                'Codigo' => 0,
                'Msg' => $msg_success
            ),
            'idProducto' => utf8_decode($IdProducto),
            'Codigo' => utf8_decode($producto['pro_codigo']),
            'Nombre' => utf8_decode($producto['pro_nombre']),
            'idFamilia' => utf8_decode($producto['pro_fpr_id']),
            'idUnidaDemedida' => utf8_decode($producto['pro_ume_id']),
            'Capacidad' => utf8_decode($producto['pro_capacidad']),
            'cantidadPack' => utf8_decode($producto['pro_cantidad_pack']),
            'baja' => utf8_decode($producto['pro_baja'])
        );
    } else {
        $aReturn = array(
            'Errs' => array(
                'Codigo' => $db->getLastErrno(),
                'Msg' => utf8_decode($db->getLastError())
            ),
            'idProducto' => utf8_decode($IdProducto),
            'Codigo' => utf8_decode($producto['pro_codigo']),
            'Nombre' => utf8_decode($producto['pro_nombre']),
            'idFamilia' => utf8_decode($producto['pro_fpr_id']),
            'idUnidaDemedida' => utf8_decode($producto['pro_ume_id']),
            'Capacidad' => utf8_decode($producto['pro_capacidad']),
            'cantidadPack' => utf8_decode($producto['pro_cantidad_pack']),
            'baja' => utf8_decode($producto['pro_baja'])
        );
    }

    return $aReturn;
}

function actualizarProductoAM($IdProducto, $producto, $Codigo, $Nombre, $idFamilia, $idUnidadDeMedida, $capacidad, $cantidadPack)
{
    //die("CANTIDADD:  " . $cantidadPack);

    global $db;

    $db->where('pro_codigo', $Codigo)
        ->update('producto', [
            'pro_id' => $IdProducto,
            'pro_nombre' => $Nombre,
            'pro_codigo' => $Codigo,
            'pro_ume_id' => $idUnidadDeMedida,
            'pro_fpr_id' => $idFamilia,
            'pro_capacidad' => $capacidad,
            'pro_cantidad_pack' => $cantidadPack
        ]);

    // Busco al vendedor recien actualizado
    $producto = $db->rawQueryOne ("select * from producto where pro_codigo='" . $producto['pro_codigo'] . "'");

    return $producto;
}

function insertarProductoAM($IdProducto, $Codigo, $Nombre, $idFamilia, $idUnidadDeMedida, $capacidad, $cantidadPack)
{
    global $db;

    $data = array(
        'pro_id' => $IdProducto,
        'pro_nombre' => $Nombre,
        'pro_codigo' => $Codigo,
        'pro_ume_id' => $idUnidadDeMedida,
        'pro_fpr_id' => $idFamilia,
        'pro_capacidad' => $capacidad,
        'pro_cantidad_pack' => $cantidadPack
    );

    $id = $db->insert('producto', $data);

    // Busco al vendedor recien insertado
    $producto = $db->rawQueryOne ("select * from producto where pro_codigo='" . $Codigo  . "'");

    return $producto;
}