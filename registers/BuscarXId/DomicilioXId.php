<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('DomicilioXId',
    [
        'login' => 'tns:login',
        'idDomicilio' => 'xsd:integer'
    ],
    array('DomicilioXidResult'=>'tns:DomicilioEntrega')
);