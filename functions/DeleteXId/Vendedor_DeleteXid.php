<?php

function Vendedor_DeleteXid($auth, $idVendedor)
{
    // Digo que voy a utilizar la variable global $mysql
    global $db;

    // Inicializo la variable resultado
    $aReturn = [];

    // Busco en la base de datos
    $vendedor = $db->rawQueryOne ('select * from vendedor where ven_id=' . $idVendedor);

    // Lo encontre
    if ($vendedor) {
        // Lo elimino
        $db->where('ven_id', $vendedor['ven_id']);

        if ($db->delete('vendedor')) {
            $cod = 2;
            $msg = 'Vendedor con id ' . $idVendedor . ' eliminado';
        } else {
            $cod = 3;
            $msg = 'Vendedor con id ' . $idVendedor . ' no se pudo eliminar';
        }
    } else {
        $cod = 1;
        $msg = 'Vendedor inexistente';
    }

    $aReturn = array(
        'Errs' => array(
            'Codigo' => $cod,
            'Msg' => $msg
        )
    );

    return $aReturn;
}