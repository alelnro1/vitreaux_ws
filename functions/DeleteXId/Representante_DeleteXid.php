<?php

function Representante_DeleteXid($auth, $idRepresentante)
{
    // Digo que voy a utilizar la variable global $mysql
    global $db;

    // Inicializo la variable resultado
    $aReturn = [];

    // Busco en la base de datos
    $representante = $db->rawQueryOne ('select * from representante where rep_id=' . $idRepresentante);

    // Lo encontre
    if ($representante) {
        // Lo elimino
        $db->where('rep_id', $representante['rep_id']);

        if ($db->delete('representante')) {
            $cod = 2;
            $msg = 'Representante con id ' . $idRepresentante . ' eliminado';
        } else {
            $cod = 3;
            $msg = 'Representante con id ' . $idRepresentante . ' no se pudo eliminar';
        }
    } else {
        $cod = 1;
        $msg = 'Representante inexistente';
    }

    $aReturn = array(
        'Errs' => array(
            'Codigo' => $cod,
            'Msg' => $msg
        )
    );

    return $aReturn;
}