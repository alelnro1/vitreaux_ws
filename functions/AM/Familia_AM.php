<?php

function Familia_AM($auth, $idFamilia, $Familia)
{
    $login = login($auth);

    if ($login !== true){
        return $login;
    }

    // Digo que voy a utilizar la variable global $mysql
    global $db;

    // Inicializo la variable resultado
    $aReturn = [];

    // Busco en la base de datos
    $familia_producto = $db->rawQueryOne ('select * from familia_producto where fpr_id=' . $idFamilia);

    if ($familia_producto) {
        // Existe => ACTUALIZO
        $familia_producto = actualizarFamilia($familia_producto, $idFamilia, $Familia);
        $msg_success = 'Modificacion exitosa';
    } else {
        // No existe => INSERTO
        $familia_producto = insertarFamilia($idFamilia, $Familia);
        $msg_success = 'Alta exitosa';
    }

    if ($db->getLastErrno() === 0) {
        $aReturn = array(
            'Errs' => array(
                'Codigo' => 0,
                'Msg' => $msg_success
            ),
            'idFamilia' => utf8_decode($familia_producto['fpr_id']),
            'familia' => utf8_decode($familia_producto['fpr_nombre']),
            'Baja' => utf8_decode($familia_producto['fpr_baja'])
        );
    } else {
        $aReturn = array(
            'Errs' => array(
                'Codigo' => $db->getLastErrno(),
                'Msg' => utf8_decode($db->getLastError())
            ),
            'idFamilia' => utf8_decode($familia_producto['fpr_id']),
            'familia' => utf8_decode($familia_producto['fpr_nombre']),
            'Baja' => utf8_decode($familia_producto['fpr_baja'])
        );
    }

    return $aReturn;
}

function actualizarFamilia($fpr, $idFamilia, $Familia)
{
    global $db;

    $db->where('fpr_id', $idFamilia)
        ->update('familia_producto', [
            'fpr_id' => $idFamilia,
            'fpr_nombre' => $Familia,
        ]);

    // Busco al domicilio recien actualizado
    $fpr = $db->rawQueryOne ('select * from familia_producto where fpr_id=' . $idFamilia);

    return $fpr;
}

function insertarFamilia($idFamilia, $Familia)
{
    global $db;

    $data = array(
        'fpr_id' => $idFamilia,
        'fpr_nombre' => $Familia,
    );

    $id = $db->insert('familia_producto', $data);

    // Busco al domicilio recien insertado
    $fpr = $db->rawQueryOne ('select * from familia_producto where fpr_id=' . $idFamilia);

    return $fpr;
}