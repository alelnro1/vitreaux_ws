<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('Representante_AM',
    [
        'login' => 'tns:login',
        'idRepresentante' => 'xsd:integer',
        'nombre' => 'xsd:string'
    ],
    array('Representante_AMResult' => 'tns:Representante_AMResult')
);