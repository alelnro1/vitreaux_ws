<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('FamiliaXId',
    [
        'login' => 'tns:login',
        'idFamilia' => 'xsd:integer'
    ],
    array('FamiliaXidResult'=>'tns:Familia')
);