<?php

function ProdCod_DeleteXcodigo($auth, $codProducto)
{
    // Digo que voy a utilizar la variable global $mysql
    global $db;

    // Inicializo la variable resultado
    $aReturn = [];

    // Busco en la base de datos
    $producto = $db->rawQueryOne ('select * from producto where pro_codigo=' . $codProducto);

    // Lo encontre
    if ($producto) {
        // Lo elimino
        $db->where('pro_codigo', $producto['pro_codigo']);

        if ($db->delete('producto')) {
            $cod = 2;
            $msg = 'Producto con codigo ' . $codProducto . ' eliminado';
        } else {
            $cod = 3;
            $msg = 'Producto con codigo ' . $codProducto . ' no se pudo eliminar';
        }
    } else {
        $cod = 1;
        $msg = 'Producto inexistente';
    }

    $aReturn = array(
        'Errs' => array(
            'Codigo' => $cod,
            'Msg' => $msg
        )
    );

    return $aReturn;
}