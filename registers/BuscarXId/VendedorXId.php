<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('VendedorXid',
    [
        'login' => 'tns:login',
        'idVendedor' => 'xsd:integer'
    ],
    array('VendedorXidResult'=>'tns:VendedorXidResult')
);