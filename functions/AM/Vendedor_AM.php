<?php

function Vendedor_AM($auth, $idVendedor, $nyap)
{


    mail("julycm92@gmail.com", 'Entro a Vendedor_AM\n 
    ', 'Salgo de Vendedor_AM\n ('. $idVendedor .')');

    $login = login($auth);

    if ($login !== true){
        return $login;
    }

    // Digo que voy a utilizar la variable global $mysql
    global $db;

    // Inicializo la variable resultado
    $aReturn = [];

    // Busco en la base de datos
    $vendedor = $db->rawQueryOne ('select * from vendedor where ven_id=' . $idVendedor);

    if ($vendedor) {
        // Existe => ACTUALIZO
        $vendedor = actualizarVendedor($vendedor, $idVendedor, $nyap);
        $msg_success = 'Modificacion exitosa';
    } else {
        // No existe => INSERTO
        $vendedor = insertarVendedor($idVendedor, $nyap);

        $msg_success = 'Alta exitosa';
    }

    if ($db->getLastErrno() === 0) {
        $aReturn = array(
            'Errs' => array(
                'Codigo' => 0,
                'Msg' => $msg_success
            ),
            'idVendedor' => utf8_decode($vendedor['ven_id']),
            'nyap' => utf8_decode($vendedor['ven_nombre']),
            'baja' => utf8_decode($vendedor['ven_baja'])
        );
    } else {
        $aReturn = array(
            'Errs' => array(
                'Codigo' => $db->getLastErrno(),
                'Msg' => utf8_decode($db->getLastError())
            ),
            'idVendedor' => utf8_decode($vendedor['ven_id']),
            'nyap' => utf8_decode($vendedor['nyap']),
            'baja' => utf8_decode($vendedor['ven_baja'])
        );
    }
    mail("julycm92@gmail.com", 'Salgo de Vendedor_AM\n 
    ', 'Salgo de Vendedor_AM\n ('. $idVendedor .')');
    return $aReturn;
}

function actualizarVendedor($vendedor, $idVendedor, $nyap)
{
    global $db;

    $db->where('ven_id', $vendedor['ven_id'])
        ->update('vendedor', [
            'ven_id' => $idVendedor,
            'ven_nombre' => $nyap
        ]);

    // Busco al vendedor recien actualizado
    $vendedor = $db->rawQueryOne ('select * from vendedor where ven_id=' . $idVendedor);

    return $vendedor;
}

function insertarVendedor($idVendedor, $nyap)
{
    global $db;

    $data = array(
        "ven_id" => $idVendedor,
        "ven_nombre" => $nyap,
    );

    $id = $db->insert('vendedor', $data);

    // Busco al vendedor recien insertado
    $vendedor = $db->rawQueryOne ('select * from vendedor where ven_id=' . $idVendedor);

    return $vendedor;
}