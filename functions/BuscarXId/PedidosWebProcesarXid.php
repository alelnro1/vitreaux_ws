<?php

function PedidosWebProcesarXid($auth, $idPedido)
{	
	mail("aleponzo1@gmail.com", 'Entro a PedidosWebProcesarXid\n', 'Entro a PedidosWebProcesarXid\n');

    $login = login($auth);

    if ($login !== true){
        return $login;
    }

    // Digo que voy a utilizar la variable global $mysql
    global $db;

    // Armo un array con los parametros y sus valores
    //$array = array_combine($nombres_parametros, $valores_parametros);

    // Inicializo la variable resultado
    $aReturn = [];

    //foreach ($array as $key => $value) {
        // Busco en la base de datos
        $pedido = $db->rawQueryOne ('select * from pedido where ped_id=' . $idPedido);

        if ($pedido){
            // Proceso el pedido
            $db->where('ped_id', $pedido['ped_id'])
                ->update('pedido', [
                    'ped_procesado' => 1,
                    'ped_fecha_procesado' => date("Y-m-d H:i:s", time())
                ]);

            if ($db->getLastErrno() === 0) {
                $aReturn = array(
                    'Errs' => array(
                        'Codigo' => 0,
                        'Msg' => ''
                    )
                );
            } else {
                $aReturn = array(
                    'Errs' => array(
                        'Codigo' => $db->getLastErrno(),
                        'Msg' => utf8_decode($db->getLastError())
                    )
                );
            }
        } else {
            $aReturn = array(
                'Codigo' => 1000,
                'Msg' => utf8_decode('No exite pedido con ese Id')
            );
        }
    //}
	
    return $aReturn;
}