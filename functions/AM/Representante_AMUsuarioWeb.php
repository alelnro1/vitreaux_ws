<?php

function Representante_AMUsuarioWeb($auth, $idRepresentante, $usuarioWeb, $contrasena, $correo, $activo)
{
    if ($activo == 'false') {
        $activo = 0;
    } else {
        $activo = 1;
    }

    $activo = (bool)$activo;
    mail("julycm92@gmail.com", 'Entro a Representante_AMUsuarioWeb\n 
    ', 'Entro a Representante_AMUsuarioWeb\n (' . $idRepresentante . ',' . $usuarioWeb . ',' . $contrasena . ',' . $correo . ',' . $activo . ')');

    $login = login($auth);

    if ($login !== true) {
        return $login;
    }

    // Digo que voy a utilizar la variable global $mysql
    global $db;

    // Inicializo la variable resultado
    $aReturn = [];

    // Busco en la base de datos


    $usuario = $db->rawQueryOne('select * from usuario where usu_login="' . $usuarioWeb . '"');
    $perfil = "Representante";
    if ($usuario) {
        // Existe => ACTUALIZO
        $activo = $correo;
        $correo = $contrasena;
        if ($activo == 'false') {
            $activo = 0;
        } else {
            $activo = 1;
        }
        $usuario = actualizarUsuario($usuario, $correo, $activo, $perfil, $usuarioWeb);

        $db->where('rep_id', $idRepresentante)
            ->update('representante', [
                'usu_id' => $usuario['usu_id'],
            ]);
        $msg_success = 'Modificacion exitosa';
    } else {

        // No existe => INSERTO
        $usuario = insertarUsuario($usuarioWeb, $contrasena, $correo, $activo, $perfil);

        $db->where('rep_id', $idRepresentante)
            ->update('representante', [
                'usu_id' => $usuario['usu_id'],
            ]);
        $msg_success = 'Alta exitosa';

    }

    if ($db->getLastErrno() === 0) {
        $aReturn = array(
            'Errs' => array(
                'Codigo' => 0,
                'Msg' => $msg_success
            ),
            'IdUsuario' => utf8_decode($usuario["usu_id"]),
            'Usuario' => utf8_decode($usuario['usu_login']),
            'Nombre' => utf8_decode($usuario['usu_nombre']),
            'Contrasena' => utf8_decode($usuario['usu_clave']),
            'Correo' => utf8_decode($usuario['usu_email']),
            'Activo' => utf8_decode($usuario['usu_activo']),
            'id' => 0,//nose que va aca
            'IdPerfil' => utf8_decode($usuario['usu_per_id']),
        );


    } else {
        $aReturn = array(
            'Errs' => array(
                'Codigo' => $db->getLastErrno(),
                'Msg' => utf8_decode($db->getLastError())
            ),
            'IdUsuario' => utf8_decode($usuario["usu_id"]),
            'Usuario' => utf8_decode($usuario['usu_login']),
            'Nombre' => utf8_decode($usuario['usu_nombre']),
            'Contrasena' => utf8_decode($usuario['usu_clave']),
            'Correo' => utf8_decode($usuario['usu_email']),
            'Activo' => utf8_decode($usuario['usu_activo']),
            'id' => 0,//nose que va aca
            'IdPerfil' => utf8_decode($usuario['usu_per_id']),
        );
    }

    mail("julycm92@gmail.com", 'Salgo de Representante_AMUsuarioWeb\n 
    ', 'Salgo de Representante_AMUsuarioWeb\n (' . $idRepresentante . ',' . $usuarioWeb . ',' . $contrasena . ',' . $correo . ',' . $activo . ')');

    return $aReturn;
}

