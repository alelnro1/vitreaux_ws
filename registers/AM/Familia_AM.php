<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('Familia_AM',
    [
        'login' => 'tns:login',
        'idFamilia' => 'xsd:integer',
        'Familia' => 'xsd:string'
    ],
    array('Familia_AMResult' => 'tns:Familia_AMResult')
);