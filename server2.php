<?php

/* Incluyo bibliotecas */
require_once('lib/nusoap.php');
require_once('config.php');
include_once('lib/mysql.class.php');

/* Creacion del server */
$server = new nusoap_server();

//$ns = 'http://www.doublepoint.com.ar/vitreaux_ws/server.php';
$ns = 'sgiWeb';
$server->configureWSDL("Service", $ns);
$server->wsdl->schemaTargetNamespace = $ns;

// Conectamos a la BD
$db = new MysqliDb (DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_NAME);

// Incluimos las funciones comunes
include_once('commons.php');

// Incluimos todas las estructuras
incluir('structs/BuscarXId');

// Incluimos las funciones que se utilizaran en los registers
incluir('functions/BuscarXId');

$server->xml_encoding = "utf-8";
$server->soap_defencoding = "utf-8";

// Registramos las funciones con sus parametros la funcion con sus parametros y habilito el servicio
incluir('registers/BuscarXId');

$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);


/**
 * Incluimos los archivos del directorio $what
 * @param $what
 */
function incluir($what)
{
    $objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($what), RecursiveIteratorIterator::SELF_FIRST);

    foreach($objects as $name => $object){
        if (is_file($object)) {
            include($object);
        }
    }
}

