<?php

function DomicilioXId($auth, $idDomicilio)
{
    $login = login($auth);

    if ($login !== true){
        return $login;
    }

    // Digo que voy a utilizar la variable global $mysql
    global $db;

    // Armo un array con los parametros y sus valores
    //$array = array_combine($nombres_parametros, $valores_parametros);

    // Inicializo la variable resultado
    $aReturn = [];

    //foreach ($array as $key => $value) {
        // Busco en la base de datos
        $domicilio = $db->rawQueryOne ('select * from domicilio where dom_id=' . $idDomicilio);

        $aReturn = array(
            'Errs' => array(
                'Codigo' => 0,
                'Msg' => ''
            ),
            'idDomicilio' => utf8_decode($domicilio['dom_id']),
            'idCliente' => utf8_decode($domicilio['dom_cli_id']),
            'Domicilio' => utf8_decode($domicilio['dom_domicilio']),
            'Baja' => utf8_decode($domicilio['dom_baja'])
        );
    //}

    return $aReturn;
}