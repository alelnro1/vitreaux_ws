<?php

/** En este archivo se definen las responses del WS */

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->wsdl->addComplexType('Errs', 'complexType', 'struct', 'all', '',
    array(
        'Codigo' => array('name'=>'Codigo','type'=>'xsd:integer'),
        'Msg' => array('name'=>'Msg','type'=>'xsd:string'),
    ));

// Creamos el tipo de registro
$server->wsdl->addComplexType('Familia_AM','complexType','struct','all','',
    array(
        'Errs' => array('name'=>'Errs','type'=>'tns:Errs'),
        'idFamilia' => array('name'=>'idFamilia','type'=>'xsd:integer'),
        'familia' => array('name'=>'familia','type'=>'xsd:string'),
        'Baja' => array('name'=>'baja','type'=>'xsd:boolean')
    ));

// Creamos el array con los registros
$server->wsdl->addComplexType('Familia_AMResult','complexType','array','','SOAP-ENC:Array',
    array(
        array(
            array('ref' => 'SOAP-ENC:arrayType',
                'wsdl:arrayType' => 'tns:Familia_AM[]'
            )
        )
    ),
    'tns:Familia_AM');