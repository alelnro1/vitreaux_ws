<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('PedidosWebSinProcesar',
    [
        'login' => 'tns:login'
    ],
    array('PedidosWebSinProcesarResult'=>'tns:PedidosWebSinProcesarResult')
	
);