<?php

/** En este archivo se definen las responses del WS */

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

// Creamos el array con los registros
$server->wsdl->addComplexType('ProductoXidResult','complexType','array','','SOAP-ENC:Array',
    array(
        array(
            array('ref' => 'SOAP-ENC:arrayType',
                'wsdl:arrayType' => 'tns:Producto[]'
            )
        )
    ),
    'tns:Producto');

// Creamos el tipo de registro
$server->wsdl->addComplexType('Producto','complexType','struct','all','',
    array(
        'Errs' => array('name'=>'Errs','type'=>'tns:Errs'),
        'idProducto' => array('name'=>'idProducto','type'=>'xsd:integer'),
        'Codigo' => array('name'=>'Codigo','type'=>'xsd:string'),
        'Nombre' => array('name'=>'codigoCliente','type'=>'xsd:string'),
        'idFamilia' => array('name'=>'idVendedor','type'=>'xsd:integer'),
        'idUnidaDemedida' => array('name'=>'idRepresentante','type'=>'xsd:integer'),
        'Capacidad' => array('name'=>'estaObservado','type'=>'xsd:double'),
        'cantidadPack' => array('name'=>'estaObservado','type'=>'xsd:integer'),
        'baja' => array('name'=>'Baja','type'=>'xsd:boolean')
    ));