<?php

function ProductoXId($auth, $idProducto)
{
    $login = login($auth);

    if ($login !== true){
        return $login;
    }

    // Digo que voy a utilizar la variable global $mysql
    global $db;

    // Armo un array con los parametros y sus valores
    //$array = array_combine($nombres_parametros, $valores_parametros);

    // Inicializo la variable resultado
    $aReturn = [];

    //foreach ($array as $key => $value) {
        // Busco en la base de datos
        $producto = $db->rawQueryOne ('select * from producto where pro_id=' . $idProducto);

        if ($producto) {
            $aReturn = array(
                'Errs' => array(
                    'Codigo' => 0,
                    'Msg' => ''
                ),
                'idProducto' => utf8_decode($producto['pro_id']),
                'Codigo' => utf8_decode($producto['pro_codigo']),
                'Nombre' => utf8_decode($producto['pro_nombre']),
                'idFamilia' => utf8_decode($producto['pro_fpr_id']),
                'idUnidaDemedida' => utf8_decode($producto['pro_ume_id']),
                'Capacidad' =>utf8_decode($producto['pro_capacidad']),
                'cantidadPack' => utf8_decode($producto['pro_cantidad_pack']),
                'baja' => utf8_decode($producto['pro_baja']),
            );
        } else {
            $aReturn = array(
                'Errs' => array(
                    'Codigo' => 1003,
                    'Msg' => 'No existe'
                ),
                'idProducto' => 0,
                'idFamilia' => 0,
                'idUnidaDemedida' => 0,
                'Capacidad' => 0,
                'cantidadPack' => 0,
                'baja' => false,
            );
        }
    //}

    return $aReturn;
}