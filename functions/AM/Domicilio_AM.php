    <?php

function Domicilio_AM($auth, $idDomicilio, $IdCliente, $domicilio)
{
    $login = login($auth);

    if ($login !== true){
        return $login;
    }

    // Digo que voy a utilizar la variable global $mysql
    global $db;

    // Inicializo la variable resultado
    $aReturn = [];

    // Busco en la base de datos
    $dom_bd = $db->rawQueryOne ('select * from domicilio where dom_id=' . $idDomicilio);

    if ($dom_bd) {
        // Existe => ACTUALIZO
        $dom_bd = actualizarDomicilio($dom_bd, $idDomicilio, $IdCliente, $domicilio);
        $msg_success = 'Modificacion exitosa';
    } else {
        // No existe => INSERTO
        $dom_bd = insertarDomicilio($idDomicilio, $IdCliente, $domicilio);
        $msg_success = 'Alta exitosa';
    }

    if ($db->getLastErrno() === 0) {
        $aReturn = array(
            'Errs' => array(
                'Codigo' => 0,
                'Msg' => $msg_success
            ),
            'idDomicilio' => utf8_decode($dom_bd['dom_id']),
            'idCliente' => utf8_decode($dom_bd['dom_cli_id']),
            'Domicilio' => utf8_decode($dom_bd['dom_domicilio']),
            'Baja' => utf8_decode($dom_bd['dom_baja']),
        );
    } else {
        $aReturn = array(
            'Errs' => array(
                'Codigo' => $db->getLastErrno(),
                'Msg' => utf8_decode($db->getLastError())
            ),
            'idDomicilio' => utf8_decode($dom_bd['dom_id']),
            'idCliente' => utf8_decode($dom_bd['dom_cli_id']),
            'Domicilio' => utf8_decode($dom_bd['dom_domicilio']),
            'Baja' => utf8_decode($dom_bd['dom_baja']),
        );
    }

    return $aReturn;
}

function actualizarDomicilio($dom_bd, $idDomicilio, $IdCliente, $domicilio)
{
    global $db;

    $db->where('dom_id', $idDomicilio)
        ->update('domicilio', [
            'dom_cli_id' => $IdCliente,
            'dom_domicilio' => $domicilio,

        ]);

    // Busco al domicilio recien actualizado
    $dom_bd = $db->rawQueryOne ('select * from domicilio where dom_id=' . $idDomicilio);

    return $dom_bd;
}

function insertarDomicilio($idDomicilio, $IdCliente, $domicilio)
{
    global $db;

    $data = array(
        'dom_id' => $idDomicilio,
        'dom_cli_id' => $IdCliente,
        'dom_domicilio' => $domicilio
    );

    $id = $db->insert('domicilio', $data);

    // Busco al domicilio recien insertado
    $dom_bd = $db->rawQueryOne ('select * from domicilio where dom_id=' . $id);

    return $dom_bd;
}