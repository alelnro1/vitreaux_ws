<?php

function UnidadesDeMedida_DeleteXid($auth, $idUnidadDeMedida)
{
    // Digo que voy a utilizar la variable global $mysql
    global $db;

    // Inicializo la variable resultado
    $aReturn = [];

    // Busco en la base de datos
    $unidad_medida = $db->rawQueryOne ('select * from unidad_medida where ume_id=' . $idUnidadDeMedida);

    // Lo encontre
    if ($unidad_medida) {
        // Lo elimino
        $db->where('ume_id', $unidad_medida['ume_id']);

        if ($db->delete('unidad_medida')) {
            $cod = 2;
            $msg = 'Unidad de Medida con id ' . $idUnidadDeMedida . ' eliminada';
        } else {
            $cod = 3;
            $msg = 'Unidad de Medida con id ' . $idUnidadDeMedida . ' no se pudo eliminar';
        }
    } else {
        $cod = 1;
        $msg = 'Unidad de Medida inexistente';
    }

    $aReturn = array(
        'Errs' => array(
            'Codigo' => $cod,
            'Msg' => $msg
        )
    );

    return $aReturn;
}