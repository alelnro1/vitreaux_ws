<?php

/** En este archivo se definen las responses del WS */

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->wsdl->addComplexType('Errs', 'complexType', 'struct', 'all', '',
    array(
        'Codigo' => array('name'=>'Codigo','type'=>'xsd:integer'),
        'Msg' => array('name'=>'Msg','type'=>'xsd:string'),
    ));

// Creamos el tipo de registro
$server->wsdl->addComplexType('Representante_AMUsuarioWeb','complexType','struct','all','',
    array(
        'Errs' => array('name'=>'Errs','type'=>'tns:Errs'),
        'IdUsuario' => array('name'=>'IdCliente','type'=>'xsd:integer'),
        'Usuario' => array('name'=>'Usuario','type'=>'xsd:string'),
        'Nombre' => array('name'=>'Nombre','type'=>'xsd:string'),
        'Correo' => array('name'=>'Correo','type'=>'xsd:string'),
        'Activo' => array('name'=>'Activo','type'=>'xsd:boolean'),
        'id' => array('name'=>'id','type'=>'xsd:integer'),
        'IdPerfil' => array('name'=>'IdPerfil','type'=>'xsd:integer'),
    ));


// Creamos el array con los registros
$server->wsdl->addComplexType('Representante_AMUsuarioWebResult','complexType','array','','SOAP-ENC:Array',
    array(
        array(
            array('ref' => 'SOAP-ENC:arrayType',
                'wsdl:arrayType' => 'tns:Representante_AMUsuarioWeb[]'
            )
        )
    ),
    'tns:Representante_AMUsuarioWeb');