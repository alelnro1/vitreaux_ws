<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('Domicilio_DeleteXid',
    [
        'login' => 'tns:login',
        'idDomicilio' => 'xsd:integer'
    ],
    array('Domicilio_DeleteXidResult'=>'tns:Domicilio_DeleteXidResult')
);