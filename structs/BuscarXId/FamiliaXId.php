<?php

/** En este archivo se definen las responses del WS */

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

// Creamos el array con los registros
$server->wsdl->addComplexType('FamiliaXidResult','complexType','array','','SOAP-ENC:Array',
    array(
        array(
            array('ref' => 'SOAP-ENC:arrayType',
                'wsdl:arrayType' => 'tns:Familia[]'
            )
        )
    ),
    'tns:Familia');

// Creamos el tipo de registro
$server->wsdl->addComplexType('Familia','complexType','struct','all','',
    array(
        'Errs' => array('name'=>'Errs','type'=>'tns:Errs'),
        'idFamilia' => array('name'=>'idFamilia','type'=>'xsd:integer'),
        'familia' => array('name'=>'familia','type'=>'xsd:string'),
        'Baja' => array('name'=>'Baja','type'=>'xsd:boolean')
    ));