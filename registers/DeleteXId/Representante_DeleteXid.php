<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('Representante_DeleteXid',
    [
        'login' => 'tns:login',
        'idRepresentante' => 'xsd:integer'
    ],
    array('Representante_DeleteXidResult'=>'tns:Representante_DeleteXidResult')
);