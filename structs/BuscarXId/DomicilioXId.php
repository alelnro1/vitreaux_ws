<?php

/** En este archivo se definen las responses del WS */

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

// Creamos el array con los registros
/*$server->wsdl->addComplexType('DomicilioXidResult','complexType','array','','SOAP-ENC:Array',
    array(
        array(
            array('ref' => 'SOAP-ENC:arrayType',
                'wsdl:arrayType' => 'tns:DomicilioEntrega[]'
            )
        )
    ),
    'tns:DomicilioEntrega');*/

// Creamos el tipo de registro
$server->wsdl->addComplexType('DomicilioEntrega','complexType','struct','all','',
    array(
        'Errs' => array('name'=>'Errs','type'=>'tns:Errs'),
        'idDomicilio' => array('name'=>'idDomicilio','type'=>'xsd:integer'),
        'idCliente' => array('name'=>'idCliente','type'=>'xsd:string'),
        'Domicilio' => array('name'=>'Domicilio','type'=>'xsd:string'),
        'Baja' => array('name'=>'Baja','type'=>'xsd:boolean')
    ));