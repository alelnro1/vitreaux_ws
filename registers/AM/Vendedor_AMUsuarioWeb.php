<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('Vendedor_AMUsuarioWeb',
    [
        'login' => 'tns:login',
        'idVendedor' => 'xsd:integer',
        'UsuarioWeb' => 'xsd:string',
        'Contrasena' => 'xsd:string',
        'Correo' => 'xsd:string',
        'Activo' => 'xsd:string'
    ],

    array('Vendedor_AMUsuarioWebResult' => 'tns:Vendedor_AMUsuarioWebResult')
);