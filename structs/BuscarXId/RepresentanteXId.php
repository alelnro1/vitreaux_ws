<?php

/** En este archivo se definen las responses del WS */

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

// Creamos el array con los registros
$server->wsdl->addComplexType('RepresentanteXidResult','complexType','array','','SOAP-ENC:Array',
    array(
        array(
            array('ref' => 'SOAP-ENC:arrayType',
                'wsdl:arrayType' => 'tns:Representante[]'
            )
        )
    ),
    'tns:Representante');

// Creamos el tipo de registro
$server->wsdl->addComplexType('Representante','complexType','struct','all','',
    array(
        'Errs' => array('name'=>'Errs','type'=>'tns:Errs'),
        'idRepresentante' => array('name'=>'idRepresentante','type'=>'xsd:integer'),
        'representante' => array('name'=>'representante','type'=>'xsd:string'),
        'baja' => array('name'=>'baja','type'=>'xsd:boolean')
    ));