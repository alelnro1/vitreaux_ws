<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('Representante_UsuarioWebXid',
    [
        'login' => 'tns:login',
        'idRepresentante' => 'xsd:integer'
    ],
    array('Representante_UsuarioWebXidResult' => 'tns:Representante_UsuarioWebXidResult')
);

