<?php

/** En este archivo se definen las responses del WS */

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->wsdl->addComplexType('Errs', 'complexType', 'struct', 'all', '',
    array(
        'Codigo' => array('name'=>'Codigo','type'=>'xsd:integer'),
        'Msg' => array('name'=>'Msg','type'=>'xsd:string'),
    ));

// Creamos el tipo de registro
$server->wsdl->addComplexType('ProdCod','complexType','struct','all','',
    array(
        'Errs' => array('name'=>'Errs','type'=>'tns:Errs'),
        'Codigo' => array('name'=>'Codigo','type'=>'xsd:integer'),
        'Nombre' => array('name'=>'Nombre','type'=>'xsd:string'),
        'Abreviado' => array('name'=>'Abreviado','type'=>'xsd:boolean')
    ));

// Creamos el array con los registros
$server->wsdl->addComplexType('ProdCod_AMResult','complexType','array','','SOAP-ENC:Array',
    array(
        array(
            array('ref' => 'SOAP-ENC:arrayType',
                'wsdl:arrayType' => 'tns:ProdCod[]'
            )
        )
    ),
    'tns:ProdCod');