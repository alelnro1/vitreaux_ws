<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('Cliente_DeleteXid',
    [
        'login' => 'tns:login',
        'idCliente' => 'xsd:integer'
    ],
    array('Cliente_DeleteXidResult'=>'tns:Cliente_DeleteXidResult')
);