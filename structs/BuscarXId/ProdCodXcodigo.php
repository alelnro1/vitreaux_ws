<?php

/** En este archivo se definen las responses del WS */

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

// Creamos el array con los registros
$server->wsdl->addComplexType('ProdCodXcodigoResult','complexType','array','','SOAP-ENC:Array',
    array(
        array(
            array('ref' => 'SOAP-ENC:arrayType',
                'wsdl:arrayType' => 'tns:ProdCod[]'
            )
        )
    ),
    'tns:ProdCod');

// Creamos el tipo de registro
$server->wsdl->addComplexType('ProdCod','complexType','struct','all','',
    array(
        'Errs' => array('name'=>'Errs','type'=>'tns:Errs'),
        'Codigo' => array('name'=>'Codigo','type'=>'xsd:string'),
        'Nombre' => array('name'=>'codigoCliente','type'=>'xsd:string'),
        'Abreviado' => array('name'=>'Abreviado','type'=>'xsd:string'),
    ));