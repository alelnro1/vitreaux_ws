<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('Producto_DeleteXid',
    [
        'login' => 'tns:login',
        'idProducto' => 'xsd:integer'
    ],
    array('Producto_DeleteXidResult'=>'tns:Producto_DeleteXidResult')
);