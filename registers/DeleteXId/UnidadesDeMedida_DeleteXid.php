<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('UnidadesDeMedida_DeleteXid',
    [
        'login' => 'tns:login',
        'idUnidadDeMedida' => 'xsd:integer'
    ],
    array('UnidadesDeMedida_DeleteXidResult'=>'tns:UnidadesDeMedida_DeleteXidResult')
);