<?php

/** En este archivo se definen las responses del WS */

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

// Creamos el array con los registros
$server->wsdl->addComplexType('UnidadeDeMedidaXidResult','complexType','array','','SOAP-ENC:Array',
    array(
        array(
            array('ref' => 'SOAP-ENC:arrayType',
                'wsdl:arrayType' => 'tns:UnidadeDeMedida[]'
            )
        )
    ),
    'tns:UnidadeDeMedida');

// Creamos el tipo de registro
$server->wsdl->addComplexType('UnidadeDeMedida','complexType','struct','all','',
    array(
        'Errs' => array('name'=>'Errs','type'=>'tns:Errs'),
        'idUnidadDeMedida' => array('name'=>'idUnidadDeMedida','type'=>'xsd:integer'),
        'unidadDeMedida' => array('name'=>'unidadDeMedida','type'=>'xsd:string'),
        'baja' => array('name'=>'baja','type'=>'xsd:boolean')
    ));