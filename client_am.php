<?php

require_once "lib/nusoap.php";

$client = new nusoap_client("http://localhost:80/vitr_soap/server.php?wsdl");

$error = $client->getError();

if ($error) {
    echo "<h2>Constructor error</h2><pre>" . $error . "</pre>";
}


/** Ejemplo de Vendedor_AM */

/*$result = $client->call("Vendedor_AM", [
    'idVendedor' => '2',
    'nyap' => 'julian marino'
]);*/

/** Ejemplo de Cliente_AM */
/*
$result = $client->call("Cliente_AM", [
    'idCliente' => '1',
    'codigoCliente' => '101011A',
    'idVendedor' => '109',
    'idRepresentante' => '119',
    'razonSocial' => 'ale ponzoA',
    'EstaObservado' => '11'
]);
*/

/** Ejemplo de Domicilio_AM */
/*
$result = $client->call("Domicilio_AM", [
    'idDomicilio' => '4',
    'IdCliente' => '4',
    'domicilio' => 'MASCAGNII',
]);
*/

/** Ejemplo de UnidadeDeMedida_AM */
/*
$result = $client->call("UnidadeDeMedida_AM", [
    'idUnidadDeMedida' => '1',
    'UnidadesDeMedida' => 'aletr',
]);
*/

/** Ejemplo de Familia_AM */
/*
$result = $client->call("Familia_AM", [
    'idFamilia' => '1',
    'Familia' => 'my family',
]);
*/

/** Ejemplo de Representante_AM */

$result = $client->call("Representante_AM", [
    'idRepresentante' => '1',
    'nombre' => 'my repe',
]);


echo "<h2>Request</h2>";
echo "<pre>" . htmlspecialchars($client->request) . "</pre>";
echo "<h2>Response</h2>";
echo "<pre>" . htmlspecialchars($client->response) . "</pre>";
