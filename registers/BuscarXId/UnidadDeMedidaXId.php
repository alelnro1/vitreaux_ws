<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('UnidadeDeMedidaXId',
    [
        'login' => 'tns:login',
        'unidadMedida' => 'xsd:integer'
    ],
    array('UnidadeDeMedidaXidResult'=>'tns:UnidadeDeMedidaXidResult')
);