<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('UnidadeDeMedida_AM',
    [
        'login' => 'tns:login',
        'idUnidadDeMedida' => 'xsd:integer',
        'UnidadesDeMedida' => 'xsd:string'
    ],
    array('UnidadeDeMedida_AMResult' => 'tns:UnidadeDeMedida_AMResult')
);