<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('Cliente_AMUsuarioWeb',
    [
        'login' => 'tns:login',
        'IdCliente' => 'xsd:integer',
        'UsuarioWeb' => 'xsd:string',
        'Contrasena' => 'xsd:string',
        'Correo' => 'xsd:string',
        'Activo' => 'xsd:string'
    ],

    array('Cliente_AMUsuarioWebResult' => 'tns:Cliente_AMUsuarioWebResult')
);