<?php

function PedidosWebSinProcesar($auth)
{
    $login = login($auth);

    if ($login !== true){
        return $login;
    }

    // Digo que voy a utilizar la variable global $mysql
    global $db;

    // Inicializo la variable resultado

    $aReturn = [];

    $aReturn['_Pedidos'] = [];

    // Busco en la base de datos
    $query = "SELECT * FROM pedido LEFT JOIN domicilio ON ped_dom_id = dom_id WHERE ped_procesado='0'";

    $pedidos_sin_procesar = $db->rawQuery ($query);

    foreach ($pedidos_sin_procesar as $key => $pedido) {
        if (empty($pedido['ped_fecha_procesado'])) {
            $fecha_procesado = date("Y-m-d", time());
        } else {
            $fecha_procesado = $pedido['ped_fecha_procesado'];
        }

        array_push(
            $aReturn['_Pedidos'],
            array(
                'Errs' => array(
                    'Codigo' => 0,
                    'Msg' => 'asd'

                ),
                'Pedido_Id' => utf8_decode($pedido['ped_id']),
                'Pedido_idCliente' => utf8_decode($pedido['dom_cli_id']),
                'Pedido_FechaInicio' => utf8_decode(date("Y-m-d", strtotime($pedido['ped_fecha_alta']))),
                'Pedido_IdDomicilio' => utf8_decode($pedido['ped_dom_id']),
                'Pedido_IdUsuario' => utf8_decode($pedido['ped_usu_id']),
                'Pedido_Procesada' => utf8_decode($pedido['ped_procesado']),
                'Pedido_FechaProcesada' => utf8_decode($fecha_procesado),
                'Pedido_Observaciones' => utf8_decode($pedido['ped_observaciones']),
                'Pedido_OrdenDeCompra' => utf8_decode($pedido['ped_orden_compra'])
            )
        );

    }

    $aReturn['Errs'] = array(
        'Codigo' => 0,
        'Msg' => 'asd'

    );

    $aReturn['Pedios_Count'] = count($pedidos_sin_procesar);

    return $aReturn;
}