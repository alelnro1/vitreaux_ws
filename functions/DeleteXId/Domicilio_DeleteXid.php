<?php

function Domicilio_DeleteXid($auth, $idDomicilio)
{
    // Digo que voy a utilizar la variable global $mysql
    global $db;

    // Armo un array con los parametros y sus valores
    //$array = array_combine($nombres_parametros, $valores_parametros);

    // Inicializo la variable resultado
    $aReturn = [];

    //foreach ($array as $key => $value) {
        // Busco en la base de datos
        $domicilio = $db->rawQueryOne ('select * from domicilio where dom_id=' . $idDomicilio);

        // Lo encontre
        if ($domicilio) {
            // Lo elimino
            $db->where('dom_id', $domicilio['dom_id']);

            if ($db->delete('domicilio')) {
                $cod = 2;
                $msg = 'Domicilio ' . $idDomicilio . ' eliminado';
            } else {
                $cod = 3;
                $msg = 'El domicilio ' . $idDomicilio . ' no se pudo eliminar';
            }
        } else {
            $cod = 1;
            $msg = 'Domicilio inexistente';
        }
    //}

    $aReturn = array(
        'Errs' => array(
            'Codigo' => $cod,
            'Msg' => $msg
        )
    );

    return $aReturn;
}