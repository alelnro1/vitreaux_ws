<?php

function Familia_DeleteXid($auth, $idFamilia)
{
    // Digo que voy a utilizar la variable global $mysql
    global $db;

    // Inicializo la variable resultado
    $aReturn = [];

    // Busco en la base de datos
    $familia = $db->rawQueryOne ('select * from familia_producto where fpr_id=' . $idFamilia);

    // Lo encontre
    if ($familia) {
        // Lo elimino
        $db->where('fpr_id', $familia['fpr_id']);

        if ($db->delete('familia_producto')) {
            $cod = 2;
            $msg = 'Familia de Productos ' . $idFamilia . ' eliminada';
        } else {
            $cod = 3;
            $msg = 'La Familia de Productos ' . $idFamilia . ' no se pudo eliminar';
        }
    } else {
        $cod = 1;
        $msg = 'Familia de Productos inexistente';
    }

    $aReturn = array(
        'Errs' => array(
            'Codigo' => $cod,
            'Msg' => $msg
        )
    );

    return $aReturn;
}