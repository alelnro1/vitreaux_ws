<?php

function ProdCod_AM($auth, $codigo, $nombre, $abreviado)
{
    $login = login($auth);

    if ($login !== true){
        return $login;
    }

    // Digo que voy a utilizar la variable global $mysql
    global $db;

    // Inicializo la variable resultado
    $aReturn = [];

    // Busco en la base de datos
    $producto = $db->rawQueryOne ("select * from producto where pro_codigo='" . $codigo . "'");

    if ($producto) {
        // Existe => ACTUALIZO
        $producto = actualizarProducto($producto, $nombre, $abreviado);
        $msg_success = 'Modificacion exitosa';
    } else {
        // No existe => INSERTO
        $producto = insertarProducto($codigo, $nombre, $abreviado);
        $msg_success = 'Alta exitosa';
    }

    if ($db->getLastErrno() === 0) {
        $aReturn = array(
            'Errs' => array(
                'Codigo' => 0,
                'Msg' => $msg_success
            ),
            'Codigo' => utf8_decode($producto['pro_codigo']),
            'Nombre' => utf8_decode($producto['pro_nombre']),
            'Abreviado' => utf8_decode('vacio')
        );
    } else {
        $aReturn = array(
            'Errs' => array(
                'Codigo' => $db->getLastErrno(),
                'Msg' => utf8_decode($db->getLastError())
            ),
            'Codigo' => utf8_decode($producto['pro_codigo']),
            'Nombre' => utf8_decode($producto['pro_nombre']),
            'Abreviado' => utf8_decode('')
        );
    }

    return $aReturn;
}

function actualizarProducto($producto, $nombre, $abreviado)
{
    global $db;

    $db->where('pro_codigo', $producto['pro_codigo'])
        ->update('producto', [
            'pro_nombre' => $nombre
        ]);

    // Busco al vendedor recien actualizado
    $vendedor = $db->rawQueryOne ("select * from producto where pro_codigo='" . $producto['pro_codigo'] . "'");

    return $vendedor;
}

function insertarProducto($codigo, $nombre, $abreviado)
{
    global $db;

    $data = array(
        "pro_codigo" => $codigo,
        "pro_nombre" => $nombre
    );

    $id = $db->insert('producto', $data);

    // Busco al vendedor recien insertado
    $vendedor = $db->rawQueryOne ("select * from producto where pro_codigo='" . $codigo  . "'");

    return $vendedor;
}