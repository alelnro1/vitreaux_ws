<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('Cliente_AM',
    [
        'login' => 'tns:login',
        'idCliente' => 'xsd:integer',
        'codigoCliente' => 'xsd:string',
        'idVendedor' => 'xsd:integer',
        'idRepresentante' => 'xsd:integer',
        'razonSocial' => 'xsd:string',
        'EstaObservado' => 'xsd:integer'
    ],
    array('Cliente_AMResult' => 'tns:Cliente_AMResult')
);