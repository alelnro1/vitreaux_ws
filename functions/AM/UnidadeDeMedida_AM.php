<?php

function UnidadeDeMedida_AM($auth, $idUnidadDeMedida, $UnidadesDeMedida)
{
    $login = login($auth);

    if ($login !== true){
        return $login;
    }

    // Digo que voy a utilizar la variable global $mysql
    global $db;

    // Inicializo la variable resultado
    $aReturn = [];

    // Busco en la base de datos
    $ume = $db->rawQueryOne ('select * from unidad_medida where ume_id=' . $idUnidadDeMedida);

    if ($ume) {
        // Existe => ACTUALIZO
        $ume = actualizarUme($ume, $idUnidadDeMedida, $UnidadesDeMedida);
        $msg_success = 'Modificacion exitosa';
    } else {
        // No existe => INSERTO
        $ume = insertarUme($idUnidadDeMedida, $UnidadesDeMedida);
        $msg_success = 'Alta exitosa';
    }

    if ($db->getLastErrno() === 0) {
        $aReturn = array(
            'Errs' => array(
                'Codigo' => 0,
                'Msg' => $msg_success
            ),
            'idUnidadDeMedida' => utf8_decode($ume['ume_id']),
            'unidadDeMedida' => utf8_decode($ume['ume_nombre']),
            'baja' => utf8_decode($ume['ume_baja']),
        );
    } else {
        $aReturn = array(
            'Errs' => array(
                'Codigo' => $db->getLastErrno(),
                'Msg' => utf8_decode($db->getLastError())
            ),
            'idUnidadDeMedida' => utf8_decode($ume['ume_id']),
            'unidadDeMedida' => utf8_decode($ume['ume_nombre']),
            'baja' => utf8_decode($ume['ume_baja']),
        );
    }

    return $aReturn;
}

function actualizarUme($ume, $idUnidadDeMedida, $UnidadesDeMedida)
{
    global $db;

    $db->where('ume_id', $idUnidadDeMedida)
        ->update('unidad_medida', [
            'ume_id' => $idUnidadDeMedida,
            'ume_nombre' => $UnidadesDeMedida,
        ]);

    // Busco al domicilio recien actualizado
    $ume = $db->rawQueryOne ('select * from unidad_medida where ume_id=' . $idUnidadDeMedida);

    return $ume;
}

function insertarUme($idUnidadDeMedida, $UnidadesDeMedida)
{
    global $db;

    $data = array(
        'ume_id' => $idUnidadDeMedida,
        'ume_nombre' => $UnidadesDeMedida
    );

    $id = $db->insert('unidad_medida', $data);

    // Busco al domicilio recien insertado
    $ume = $db->rawQueryOne ('select * from unidad_medida where ume_id=' . $idUnidadDeMedida);

    return $ume;
}