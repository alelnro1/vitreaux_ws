<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('Domicilio_DeleteXidCliente',
    [
        'login' => 'tns:login',
        'idCliente' => 'xsd:integer'
    ],
    array('Domicilio_DeleteXidClienteResult'=>'tns:Domicilio_DeleteXidClienteResult')
);