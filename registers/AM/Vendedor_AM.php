<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('Vendedor_AM',
    [
        'login' => 'tns:login',
        'idVendedor' => 'xsd:integer',
        'nyap' => 'xsd:string'
    ],
    array('Vendedor_AMResult' => 'tns:Vendedor_AMResult')
);