<?php

/** En este archivo se definen las responses del WS */

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;


$server->wsdl->addComplexType('VendedorXidResult','complexType','array','','SOAP-ENC:Array',
    array(
        array(
            array('ref' => 'SOAP-ENC:arrayType',
                'wsdl:arrayType' => 'tns:Vendedor[]'
            )
        )
    ),
    'tns:Vendedor');

// Creamos el tipo de registro
$server->wsdl->addComplexType('Vendedor','complexType','struct','all','',
    array(
        'Errs' => array('name'=>'Errs','type'=>'tns:Errs'),
        'idVendedor' => array('name'=>'idVendedor','type'=>'xsd:integer'),
        'nyap' => array('name'=>'nyap','type'=>'xsd:string'),
        'baja' => array('name'=>'baja','type'=>'xsd:boolean')
    ));