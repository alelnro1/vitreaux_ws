<?php

function Producto_DeleteXid($auth, $idProducto)
{
    // Digo que voy a utilizar la variable global $mysql
    global $db;

    // Inicializo la variable resultado
    $aReturn = [];

    // Busco en la base de datos
    $producto = $db->rawQueryOne ('select * from producto where pro_id=' . $idProducto);

    // Lo encontre
    if ($producto) {
        // Lo elimino
        $db->where('pro_id', $producto['pro_id']);

        if ($db->delete('producto')) {
            $cod = 2;
            $msg = 'Producto ' . $idProducto . ' eliminado';
        } else {
            $cod = 3;
            $msg = 'Producto ' . $idProducto . ' no se pudo eliminar';
        }
    } else {
        $cod = 1;
        $msg = 'Producto inexistente';
    }

    $aReturn = array(
        'Errs' => array(
            'Codigo' => $cod,
            'Msg' => $msg
        )
    );

    return $aReturn;
}