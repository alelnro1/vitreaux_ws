<?php

$server->wsdl->addComplexType('Errs', 'complexType', 'struct', 'all', '',
    array(
        'Codigo' => array('name'=>'Codigo','type'=>'xsd:integer'),
        'Msg' => array('name'=>'Msg','type'=>'xsd:string'),
    ));