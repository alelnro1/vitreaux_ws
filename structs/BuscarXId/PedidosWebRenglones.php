<?php

/** En este archivo se definen las responses del WS */

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->wsdl->addComplexType('Err', 'complexType', 'struct', 'all', '',
    array(
        'Codigo' => array('name'=>'Codigo','type'=>'xsd:integer'),
        'Msg' => array('name'=>'Msg','type'=>'xsd:string')
    ));

$server->wsdl->addComplexType('Autenticacion', 'complexType', 'struct', 'all', '',
    array(
        'Usuario' => array('name'=>'Usuario','type'=>'xsd:string'),
        'Password' => array('name'=>'Password','type'=>'xsd:string'),
        'Url' => array('name'=>'Url','type'=>'xsd:string'),
    ));

// Creamos el tipo de registro
$server->wsdl->addComplexType('PedidosWebRenglones','complexType','struct','all','',
    array(
        '_PedidosRenglones' => array('name' => '_PedidosRenglones', 'type' => 'tns:_PedidosRenglones')
    ));

$server->wsdl->addComplexType('_PedidosRenglones','complexType','struct','all','',
    array(
        'Renglones' => array('name' => 'Renglones', 'type' => 'tns:ArrayOf_Renglon'),
        'Errs' => array('name'=>'Errs','type'=>'tns:Err'),
        'Count' => array('name'=>'Count','type'=>'xsd:integer'),
    ));

$server->wsdl->addComplexType('ArrayOf_Renglon', 'complexType', 'struct', 'all', '',
    array(
        '_Renglon' => array('name'=>'_Renglon','type'=>'tns:_Renglon')
    ));

$server->wsdl->addComplexType('_Renglon', 'complexType', 'struct', 'all', '',
    array(
        'idPedidoWeb' => array('name'=>'idPedidoWeb','type'=>'xsd:integer'),
        'idProducto' => array('name'=>'idProducto','type'=>'xsd:integer'),
        'Cantidad' => array('name'=>'Cantidad','type'=>'xsd:integer'),
        'fechas' => array('name'=>'fechas','type'=>'xsd:dateTime'),
        'Comentarios' => array('name'=>'Comentarios','type'=>'xsd:string'),
        'Errs ' => array('name'=>'Errs ','type'=>'tns:Err'),

    ));

$server->wsdl->addComplexType('PedidosWebRenglonesResult','complexType','array','','SOAP-ENC:Array',
    array(
        array(
            array('ref' => 'SOAP-ENC:arrayType',
                'wsdl:arrayType' => 'tns:PedidosWebRenglones[]'
            )
        )
    ),
    'tns:PedidosWebRenglones');