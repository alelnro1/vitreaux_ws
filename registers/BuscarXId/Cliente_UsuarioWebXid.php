<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('Cliente_UsuarioWebXid',
    [
        'login' => 'tns:login',
        'IdCliente' => 'xsd:integer'
    ],
    array('Cliente_UsuarioWebXidResult' => 'tns:Cliente_UsuarioWebXidResult')
);

