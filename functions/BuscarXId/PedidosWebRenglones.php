<?php

function PedidosWebRenglones($auth, $idPedido)
{
    $login = login($auth);

    if ($login !== true){
        return $login;
    }

    // Digo que voy a utilizar la variable global $mysql
    global $db;

    // Inicializo la variable resultado

    $aReturn = [];

    $aReturn['Renglones'] = [];

    // Busco en la base de datos
    $query = "SELECT * FROM pedido_detalle LEFT JOIN producto ON pde_pro_id = pro_id WHERE pde_ped_id ='" . $idPedido ."'";

    $renglones = $db->rawQuery ($query);

    foreach ($renglones as $key => $renglon) {
        array_push(
            $aReturn['Renglones'],
            array(
                'Errs' => array(
                    'Codigo' => 0,
                    'Msg' => 'nada'

                ),
                'idPedidoWeb' => utf8_decode($renglon['pde_ped_id']),
                'idProducto' => utf8_decode($renglon['pde_pro_id']),
                'Cantidad' => utf8_decode($renglon['pde_cantidad']),
                //'fechas' => utf8_decode(date("Y-m-d", strtotime($renglon['pde_fecha']))),
                'fechas' => utf8_decode(date("Y-m-d", strtotime($renglon['pde_fecha']))),
                'Comentarios' => utf8_decode($renglon['pde_comentario']),
            )
        );

    }

    $aReturn['Errs'] = array(
        'Codigo' => 0,
        'Msg' => 'asd'

    );

    $aReturn['Count'] = count($renglones);

    return $aReturn;
}