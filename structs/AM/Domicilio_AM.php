<?php

/** En este archivo se definen las responses del WS */

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

// Creamos el tipo de registro
$server->wsdl->addComplexType('Domicilio_AM','complexType','struct','all','',
    array(
        'Errs' => array('name'=>'Errs','type'=>'tns:Errs'),
        'idDomicilio' => array('name'=>'idDomicilio','type'=>'xsd:integer'),
        'IdCliente' => array('name'=>'IdCliente','type'=>'xsd:string'),
        'Domicilio' => array('name'=>'Domicilio','type'=>'xsd:string'),
        'Baja' => array('name'=>'Baja','type'=>'xsd:boolean')
    ));

// Creamos el array con los registros
$server->wsdl->addComplexType('Domicilio_AMResult','complexType','array','','SOAP-ENC:Array',
    array(
        array(
            array('ref' => 'SOAP-ENC:arrayType',
                'wsdl:arrayType' => 'tns:Domicilio_AM[]'
            )
        )
    ),
    'tns:Domicilio_AM');