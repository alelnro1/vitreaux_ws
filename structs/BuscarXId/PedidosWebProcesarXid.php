<?php

/** En este archivo se definen las responses del WS */

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

// Creamos el array con los registros
$server->wsdl->addComplexType('PedidosWebProcesarXidResult','complexType','array','','SOAP-ENC:Array',
    array(
        array(
            array('ref' => 'SOAP-ENC:arrayType',
                'wsdl:arrayType' => 'tns:PedidoWebProcesarXid[]'
            )
        )
    ),
    'tns:PedidoWebProcesarXid');

// Creamos el tipo de registro
$server->wsdl->addComplexType('PedidoWebProcesarXid','complexType','struct','all','',
    array(
        'Errs' => array('name'=>'Errs','type'=>'tns:Errs'),
    ));