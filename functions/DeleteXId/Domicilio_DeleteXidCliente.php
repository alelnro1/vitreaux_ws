<?php

function Domicilio_DeleteXidCliente($auth, $idCliente)
{
    // Digo que voy a utilizar la variable global $mysql
    global $db;

    // Inicializo la variable resultado
    $aReturn = [];

    // Busco en la base de datos
    $domicilio = $db->rawQueryOne ('select * from domicilio where dom_cli_id=' . $idCliente);

    // Lo encontre
    if ($domicilio) {
        // Lo elimino
        $db->where('dom_cli_id', $domicilio['dom_cli_id']);

        if ($db->delete('domicilio')) {
            $cod = 2;
            $msg = 'Domicilio del cliente ' . $idCliente . ' eliminado';
        } else {
            $cod = 3;
            $msg = 'El domicilio del cliente' . $idCliente . ' no se pudo eliminar';
        }
    } else {
        $cod = 1;
        $msg = 'Domicilio de cliente inexistente';
    }

    $aReturn = array(
        'Errs' => array(
            'Codigo' => $cod,
            'Msg' => $msg
        )
    );

    return $aReturn;
}