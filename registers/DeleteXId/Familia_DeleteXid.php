<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('Familia_DeleteXid',
    [
        'login' => 'tns:login',
        'idFamilia' => 'xsd:integer'
    ],
    array('Familia_DeleteXidResult'=>'tns:Familia_DeleteXidResult')
);