<?php

require_once "lib/nusoap.php";

$client = new nusoap_client("http://doublepoint.com.ar/vitreaux_ws/server.php?wsdl");

$error = $client->getError();

if ($error) {
    echo "<h2>Constructor error</h2><pre>" . $error . "</pre>";
}

/** Ejemplo de ClienteXId */
    $result = $client->call("PedidosWebSinProcesar", [
        'Autenticacion' => [
            'Usuario' => 'ale',
            'Password' => '123',
            'url' => 'http://doublepoint.com.ar/vitreaux_ws/server.php?wsdl'
        ]
    ]);

echo "<h2>Request</h2>";
echo "<pre>" . htmlspecialchars($client->request) . "</pre>";
echo "<h2>Response</h2>";
echo "<pre>" . htmlspecialchars($client->response) . "</pre>";
