<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('Representante_AMUsuarioWeb',
    [
        'login' => 'tns:login',
        'idRepresentante' => 'xsd:integer',
        'UsuarioWeb' => 'xsd:string',
        'Contrasena' => 'xsd:string',
        'Correo' => 'xsd:string',
        'Activo' => 'xsd:string'
    ],

    array('Representante_AMUsuarioWebResult' => 'tns:Representante_AMUsuarioWebResult')
);