<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('Producto_AM',
    [
        'login' => 'tns:login',
        'IdProducto' => 'xsd:integer',
        'Codigo' => 'xsd:string',
        'Nombre' => 'xsd:string',
        'idFamilia' => 'xsd:integer',
        'idUnidadDeMedida' => 'xsd:integer',
        'capacidad' => 'xsd:integer',
        'cantidadPack' => 'xsd:integer'
    ],
    array('Producto_AMResult' => 'tns:Producto_AMResult')
);