<?php

/** En este archivo se definen las responses del WS */

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->wsdl->addComplexType('Errs', 'complexType', 'struct', 'all', '',
    array(
        'Codigo' => array('name'=>'Codigo','type'=>'xsd:integer'),
        'Msg' => array('name'=>'Msg','type'=>'xsd:string'),
    ));

// Creamos el tipo de registro
$server->wsdl->addComplexType('Productoo','complexType','struct','all','',
    array(
        'Errs' => array('name'=>'Errs','type'=>'tns:Errs'),
        'idProducto' => array('name'=>'idProducto','type'=>'xsd:integer'),
        'Codigo' => array('name'=>'Codigo','type'=>'xsd:string'),
        'Nombre' => array('name'=>'Nombre','type'=>'xsd:boolean'),
        'idFamilia' => array('name'=>'idFamilia','type'=>'xsd:integer'),
        'idUnidaDemedida' => array('name'=>'idUnidaDemedida','type'=>'xsd:integer'),
        'Capacidad' => array('name'=>'Capacidad','type'=>'xsd:double'),
        'cantidadPack' => array('name'=>'cantidadPack','type'=>'xsd:integer'),
        'baja' => array('name'=>'baja','type'=>'xsd:boolean')
    ));

// Creamos el array con los registros
$server->wsdl->addComplexType('Producto_AMResult','complexType','array','','SOAP-ENC:Array',
    array(
        array(
            array('ref' => 'SOAP-ENC:arrayType',
                'wsdl:arrayType' => 'tns:Productoo[]'
            )
        )
    ),
    'tns:Productoo');