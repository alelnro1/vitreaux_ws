<?php

/** En este archivo se definen las responses del WS */

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->wsdl->addComplexType('Err', 'complexType', 'struct', 'all', '',
    array(
        'Codigo' => array('name'=>'Codigo','type'=>'xsd:integer'),
        'Msg' => array('name'=>'Msg','type'=>'xsd:string')
    ));
	
$server->wsdl->addComplexType('Autenticacion', 'complexType', 'struct', 'all', '',
    array(
        'Usuario' => array('name'=>'Usuario','type'=>'xsd:string'),
        'Password' => array('name'=>'Password','type'=>'xsd:string'),
		'Url' => array('name'=>'Url','type'=>'xsd:string'),
    ));
	

	
$server->wsdl->addComplexType('_Pedido', 'complexType', 'struct', 'all', '',
    array(
		'Errs' => array('name'=>'Errs','type'=>'tns:Err'),
        'Pedido_Id' => array('name'=>'Pedido_Id','type'=>'xsd:integer'),
        'Pedido_idCliente' => array('name'=>'Pedido_idCliente','type'=>'xsd:integer'),
        'Pedido_FechaInicio' => array('name'=>'Pedido_FechaInicio','type'=>'xsd:dateTime'),
        'Pedido_IdDomicilio' => array('name'=>'Pedido_IdDomicilio','type'=>'xsd:integer'),
        'Pedido_IdUsuario' => array('name'=>'Pedido_IdUsuario','type'=>'xsd:integer'),
        'Pedido_Procesada' => array('name'=>'Pedido_Procesada','type'=>'xsd:boolean'),
        'Pedido_FechaProcesada' => array('name'=>'Pedido_FechaProcesada','type'=>'xsd:dateTime'),
        'Pedido_Observaciones' => array('name'=>'Pedido_Observaciones','type'=>'xsd:string'),
        'Pedido_OrdenDeCompra' => array('name'=>'Pedido_OrdenDeCompra','type'=>'xsd:string')
		
    ));

// Creamos el tipo de registro
$server->wsdl->addComplexType('PedidosWebSinProcesar','complexType','struct','all','',
    array(
		'_Pedidos_Resul' => array('name' => '_Pedidos', 'type' => 'tns:_Pedidos_Resul')
    ));
	
$server->wsdl->addComplexType('_Pedidos_Resul','complexType','struct','all','',
    array(
		'_Pedidos' => array('name' => '_Pedidos', 'type' => 'tns:ArrayOf_Pedido'),
        'Errs' => array('name'=>'Errs','type'=>'tns:Err'),
        'Pedios_Count' => array('name'=>'Pedios_Count','type'=>'xsd:integer'),
    ));

$server->wsdl->addComplexType('ArrayOf_Pedido', 'complexType', 'struct', 'all', '',
    array(
        '_Pedido' => array('name'=>'_Pedido','type'=>'tns:_Pedido')
    ));
	
$server->wsdl->addComplexType('PedidosWebSinProcesarResult','complexType','array','','SOAP-ENC:Array',
	array(
		array(
			array('name' => 'test', 'ref' => 'SOAP-ENC:arrayType',
				'wsdl:arrayType' => 'tns:PedidosWebSinProcesar[]'
			)
		)
	),
    'tns:PedidosWebSinProcesar');