<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('Vendedor_DeleteXid',
    [
        'login' => 'tns:login',
        'idVendedor' => 'xsd:integer'
    ],
    array('Vendedor_DeleteXidResult'=>'tns:Vendedor_DeleteXidResult')
);