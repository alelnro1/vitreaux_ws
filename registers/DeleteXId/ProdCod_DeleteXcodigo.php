<?php

// Decimos que vamos a utilizar la variable global $server definida en otro lado
global $server;

$server->register('ProdCod_DeleteXcodigo',
    [
        'login' => 'tns:login',
        'codigo' => 'xsd:string'
    ],
    array('ProdCod_DeleteXcodigoResult'=>'tns:ProdCod_DeleteXcodigoResult')
);